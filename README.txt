ABOUT MINIMA
-----------
Minima is a custom base theme. New minimal Drupal 8 theme based on Bootstrap 3.

To use Minima as your base theme, set the 'base theme' in your theme's .info.yml file to "minima":
  base theme: Minima


See https://www.drupal.org/project/minima
for more information on using the Minima theme.

ABOUT DRUPAL THEMING
--------------------

See https://www.drupal.org/docs/8/theming for more information on Drupal theming.